package com.fasthamster.flyingcubeslw.android;

import android.content.Context;
import android.content.SharedPreferences;

import com.fasthamster.flyingcubeslw.FlyingCubesLW;

/**
 * Created by alex on 11.03.16.
 */

public class SettingsHandler  {

    Context context;
    SharedPreferences settings;
    SharedPreferences.Editor editor;

    private boolean direction;
    private boolean mask;
    private int resolution;
    private int speed;
    private int color;


    public SettingsHandler(Context context) {
        this.context = context;
        load();
    }

    public void load() {

        settings = context.getSharedPreferences(FlyingCubesLW.PREFS_NAME, 0);
        direction = settings.getBoolean(FlyingCubesLW.DIRECTION_ALIAS, FlyingCubesLW.DIRECTION_DEFAULT);
        mask = settings.getBoolean(FlyingCubesLW.MASK_ALIAS, FlyingCubesLW.MASK_DEFAULT);
        speed = settings.getInt(FlyingCubesLW.SPEED_ALIAS, FlyingCubesLW.SPEED_DEFAULT);
        color = settings.getInt(FlyingCubesLW.COLOR_ALIAS, FlyingCubesLW.COLOR_DEFAULT);
        resolution = settings.getInt(FlyingCubesLW.RESOLUTION_ALIAS, FlyingCubesLW.RESOLUTION_DEFAULT);

    }

    public void save() {

        editor = settings.edit();
        editor.putBoolean(FlyingCubesLW.DIRECTION_ALIAS, direction);
        editor.putBoolean(FlyingCubesLW.MASK_ALIAS, mask);
        editor.putInt(FlyingCubesLW.SPEED_ALIAS, speed);
        editor.putInt(FlyingCubesLW.COLOR_ALIAS, color);
        editor.putInt(FlyingCubesLW.RESOLUTION_ALIAS, resolution);
        editor.commit();

    }

    public boolean getDirection() {
        return direction;
    }

    public boolean getMask() {
        return mask;
    }

    public int getSpeed() {
        return speed;
    }

    public int getColor() {
        return color;
    }

    public int getResolution() {
        return resolution;
    }

    public void setDirection(boolean d) {
        this.direction = d;
    }

    public void setMask(boolean m) {
        this.mask = m;
    }

    public void setSpeed(int s) {
        this.speed = s;
    }

    public void setColor(int c) {
        this.color = c;
    }

    public void setResolution(int r) {
        this.resolution = r;
    }
}
