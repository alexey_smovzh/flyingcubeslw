package com.fasthamster.flyingcubeslw.android;

import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.badlogic.gdx.backends.android.AndroidWallpaperListener;
import com.fasthamster.flyingcubeslw.FlyingCubesLW;

/**
 * Created by alex on 11.03.16.
 */

public class LiveWallpaper extends AndroidLiveWallpaperService {

    private FlyingCubesLWListener listener;
    public static SettingsHandler settings;


    @Override
    public void onCreateApplication() {

        super.onCreateApplication();

        settings = new SettingsHandler(getApplicationContext());

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useCompass = false;
        config.useWakelock = false;
        config.useAccelerometer = false;
        config.getTouchEventsForLiveWallpaper = false;

        listener = new FlyingCubesLWListener();

        initialize(listener, config);

    }

    @Override
    public Engine onCreateEngine() {
        return new AndroidWallpaperEngine(){
            @Override
            public void onPause(){
                super.onPause();
                listener.pause();
            }

            @Override
            public void onResume(){
                super.onResume();

                // Ads staff
//                AdsDialogAsync dialog = new AdsDialogAsync(getApplicationContext());
//                dialog.execute();
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public static class FlyingCubesLWListener extends FlyingCubesLW implements AndroidWallpaperListener {


        @Override
        public void offsetChange(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {

        }

        @Override
        public void previewStateChange(boolean isPreview) {

        }
    }
}
