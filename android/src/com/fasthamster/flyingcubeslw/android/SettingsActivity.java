package com.fasthamster.flyingcubeslw.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.fasthamster.flyingcubeslw.FlyingCubesLW;

/**
 * Created by alex on 11.03.16.
 */

public class SettingsActivity extends Activity implements SeekBar.OnSeekBarChangeListener,
                                                          AdapterView.OnItemSelectedListener,
                                                          Button.OnClickListener {

    CheckBox direction;
    CheckBox mask;
    CheckBox resolution;
    SeekBar seekBar;
    TextView result;
    Spinner colorSpinner;
    Button rateMe;

    private ColorSpinnerAdapter colorAdapter;

    private boolean resolutionChanged = false;              // Flag to track resolution change


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Layouts
        setContentView(R.layout.settings);
        direction = (CheckBox)findViewById(R.id.chk_direction);
        mask = (CheckBox)findViewById(R.id.chk_mask);
        resolution = (CheckBox)findViewById(R.id.chk_resolution);
        seekBar = (SeekBar)findViewById(R.id.seek_bar);
        result = (TextView)findViewById(R.id.tv_result);
        colorSpinner = (Spinner)findViewById(R.id.sp_color);
        rateMe = (Button)findViewById(R.id.btn_rate);

        // Adapters
        colorAdapter = new ColorSpinnerAdapter(SettingsActivity.this);
        colorSpinner.setAdapter(colorAdapter);

        // Listeners
        seekBar.setOnSeekBarChangeListener(this);
        colorSpinner.setOnItemSelectedListener(this);
        rateMe.setOnClickListener(this);

        // Direction checkbox listener
        direction.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LiveWallpaper.settings.setDirection(isChecked);
            }
        });

        // Mask checkbox listener
        mask.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LiveWallpaper.settings.setMask(isChecked);
            }
        });

        // Resolution checkbox listener
        resolution.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    if (LiveWallpaper.settings.getResolution() != FlyingCubesLW.HIRES) {     // Resolution changed
                        LiveWallpaper.settings.setResolution(FlyingCubesLW.HIRES);
                        resolutionChanged = true;
                    }
                } else {
                    if (LiveWallpaper.settings.getResolution() != FlyingCubesLW.LOWRES) {    // Resolution changed
                        LiveWallpaper.settings.setResolution(FlyingCubesLW.LOWRES);
                        resolutionChanged = true;
                    }
                }
            }
        });

        // States
        direction.setChecked(LiveWallpaper.settings.getDirection());
        mask.setChecked(LiveWallpaper.settings.getMask());
        seekBar.setProgress(LiveWallpaper.settings.getSpeed());
        colorSpinner.setSelection(colorAdapter.getItemPosition(LiveWallpaper.settings.getColor()));
        resolution.setChecked(LiveWallpaper.settings.getResolution() == FlyingCubesLW.HIRES ? true : false);

    }

    @Override
    protected void onStop() {

        super.onStop();

        if(resolutionChanged == true) {
            FlyingCubesLW.setResolution(LiveWallpaper.settings.getResolution());
            resolutionChanged = false;
        }

        FlyingCubesLW.setDirection(LiveWallpaper.settings.getDirection());
        FlyingCubesLW.setMask(LiveWallpaper.settings.getMask());
        FlyingCubesLW.setSpeed(LiveWallpaper.settings.getSpeed());
        FlyingCubesLW.setColor(LiveWallpaper.settings.getColor());
        LiveWallpaper.settings.save();

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        LiveWallpaper.settings.setSpeed(progress);
        result.setText(Integer.toString(progress));

    }

    // Rate me button handler
    @Override
    public void onClick(View v) {

        Intent market = new Intent(Intent.ACTION_VIEW);
        market.setData(Uri.parse("market://details?id=" + this.getPackageName()));

        Intent website = new Intent(Intent.ACTION_VIEW);
        website.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + this.getPackageName()));

        try {
            startActivity(market);
        } catch (ActivityNotFoundException e) {
            startActivity(website);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        LiveWallpaper.settings.setColor(ColorSpinnerAdapter.colors[position]);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
