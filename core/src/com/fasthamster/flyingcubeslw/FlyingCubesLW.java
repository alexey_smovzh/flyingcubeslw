package com.fasthamster.flyingcubeslw;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

// TODO: ads staff !!!

// convert video recorded from android studio from variable frame rate to constant
// ffmpeg -i video.mp4 -c:v libx264 -preset ultrafast -crf 30 -threads 8 -c:a copy video_fixed.mp4

// Stickers
// high resolution
// change direction
// apply mask
// change speed
// background color
// low battery consumption

public class FlyingCubesLW extends ApplicationAdapter {

    private OrthographicCamera camera;
    private SpriteBatch batch;
    private TextureAtlas atlas;
    private Texture pattern;
    private Sprite spiral;
    private boolean paused;

    private String currentAtlasKey = "0001";
    private int currentResolution = NORES;

    private static int currentFrame;
    private static int startFrame;
    private static int increment;
    private static int target;

    private static int speed = 30;
    private static float r, g, b;
    private static boolean mask;
    private static int resolution;

    private int posX;                   // Sprite position
    private int posY;
    private int width;
    private int height;

    private static final float WIDTH = 532f;
    private static final float HEIGHT = 818f;
    public static final int NORES = 0;
    public static final int HIRES = 1;
    public static final int LOWRES = 2;

    public static final String PREFS_NAME = "settings";
    public static final String SPEED_ALIAS = "speed";
    public static final String DIRECTION_ALIAS = "direction";
    public static final String MASK_ALIAS = "mask";
    public static final String COLOR_ALIAS = "color";
    public static final String RESOLUTION_ALIAS = "resolution";
    public static final int SPEED_DEFAULT = 30;
    public static final boolean DIRECTION_DEFAULT = false;
    public static final boolean MASK_DEFAULT = true;
    public static final int COLOR_DEFAULT = (0xFF << 24) | (63 << 16) | (63 << 8) | 63;
    public static final int RESOLUTION_DEFAULT = LOWRES;


    @Override
	public void create () {

        camera = new OrthographicCamera();
		batch = new SpriteBatch();

        pattern = new Texture(Gdx.files.internal("pattern.png"));
        pattern.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);

        Preferences preferences = Gdx.app.getPreferences(PREFS_NAME);
        setSpeed(preferences.getInteger(SPEED_ALIAS, SPEED_DEFAULT));
        setDirection(preferences.getBoolean(DIRECTION_ALIAS, DIRECTION_DEFAULT));
        setMask(preferences.getBoolean(MASK_ALIAS, MASK_DEFAULT));
        setColor(preferences.getInteger(COLOR_ALIAS, COLOR_DEFAULT));
        setResolution(preferences.getInteger(RESOLUTION_ALIAS, RESOLUTION_DEFAULT));

        paused = false;
	}

    // Clockwise or counterclockwise triangle rotation direction, depends of frames view order
    private static void setDirectionVars(boolean d) {

        if(d) {
            startFrame = 1;
            currentFrame = 1;
            increment = 1;
            target = 33;
        } else {
            startFrame = 33;
            currentFrame = 33;
            increment = -1;
            target = 1;
        }
    }

    private void setupAtlas(int r) {

        if(atlas != null) atlas.dispose();

        atlas = r == HIRES ? new TextureAtlas(Gdx.files.internal("spiral.atlas")) :
                             new TextureAtlas(Gdx.files.internal("spirallow.atlas"));

        TextureAtlas.AtlasRegion region = atlas.findRegion(currentAtlasKey);
        spiral = new Sprite(region);

        currentResolution = r;
    }

    // Setters
    public static void setDirection(boolean d) {
        setDirectionVars(d);
    }

    public static void setMask(boolean m) {
        FlyingCubesLW.mask = m;
    }

    public static void setSpeed(int s) {
        FlyingCubesLW.speed = s;
    }

    public static void setColor(int color) {
        FlyingCubesLW.r = (float)(color >>> 16 & 0xFF) / 255;
        FlyingCubesLW.g = (float)(color >>> 8 & 0xFF) / 255;
        FlyingCubesLW.b = (float)(color & 0xFF) / 255;
    }

    public static void setResolution(int r) {
        FlyingCubesLW.resolution = r;
    }

	@Override
	public void render () {

        if(currentResolution != resolution)
            setupAtlas(resolution);

        if(!paused) {
            Gdx.gl20.glClearColor(r, g, b, 1f);
            Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

            if (speed != 0) {
                currentFrame += increment;
                if (currentFrame == target)
                    currentFrame = startFrame;

                currentAtlasKey = String.format("%04d", currentFrame);
                spiral.setRegion(atlas.findRegion(currentAtlasKey));

                sleep(speed);
            }

            batch.setProjectionMatrix(camera.combined);

            batch.begin();

            if(mask)
                batch.draw(pattern, 0, 0, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

            batch.draw(spiral, posX, posY, width, height);

            batch.end();

        }
	}

    // Sleep for power consumption
    private long diff, start = System.currentTimeMillis();
    private void sleep(int fps) {

        diff = System.currentTimeMillis() - start;
        long targetDelay = 1000 / fps;
        if(diff < targetDelay) {
            try {
                Thread.sleep(targetDelay - diff);
            } catch (InterruptedException e) {	}
        }
        start = System.currentTimeMillis();
    }

    // From libgdx Scaling class source
    private void fit(int w, int h) {

        float aspect;

        if(w < h) {                         // Vertical
            aspect = w > WIDTH ? (float) w / WIDTH : WIDTH / (float)w;
        } else {
            aspect = h < HEIGHT ? (float) h / HEIGHT : HEIGHT / (float) h;
        }

        width = (int)(WIDTH * aspect);
        height = (int)(HEIGHT * aspect);

    }

    private void setSpriteCoord(int w, int h) {
        posX = w / 2 - width / 2;                         // screen center point shift to half sprite size
        posY = h / 2 - height / 2;
    }

    @Override
    public void resize(int w, int h) {

        fit(w, h);
        setSpriteCoord(w, h);

        camera.setToOrtho(false, w, h);
        camera.update();
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        paused = false;
    }

    @Override
    public void dispose() {

        if(batch != null) batch.dispose();
        if(atlas != null) atlas.dispose();
        if(pattern != null) pattern.dispose();

    }
}
